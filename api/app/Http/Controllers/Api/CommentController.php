<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use App\Models\Post;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$post = Post::where('slug', $request->post)->firstOrFail();
		
		return CommentResource::collection($post->comments()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request, Comment $comment)
    {
        $post = Post::where('slug', $request->post)->firstOrFail();

        $commentdata = $request->validated();
        $commentdata['commentable_type'] = 'App\Models\Post';
        $commentdata['post_id'] = $post->id;
        $commentdata['user_id'] = $request->user()->id;

        $result = $comment->create($commentdata);

        if( $result->exists ) {
			return response()->json(new CommentResource($result), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $post = Post::where('slug', $request->post)->firstOrFail();

            $comment = $post->comments()->findOrFail($request->comment);
			$result = $comment->update($request->all());
			
			return response()->json(new CommentResource($comment), 200);
			
        } catch (Exception $exception) {
			return response()->json([
				'message' => $exception->getMessage()
			], 404);
			
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $post = Post::where('slug', $request->post)->firstOrFail();
            $comment = $post->comments()->findOrFail($request->comment);
            $result = $comment->delete($request->comment);

			return response()->json(['status' => 'record deleted successfully'], 200);
			
        } catch (Exception $exception) {
			return response()->json([
				'message' => $exception->getMessage()
			], 404);
			
        }
    }
}
