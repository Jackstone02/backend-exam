<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Logged out logged in user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
    }
}
