<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle the registration of new users as well as their validation and creation.
     *
     * @param  \App\Http\Requests\RegisterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterRequest $request)
    {
        $user = User::create(
			array_merge($request->validated(), ['password' => Hash::make($request->password)]
		));
		
		$response = [
			'name' => $user->name,
            'email' => $user->email,
            'updated_at' => Carbon::parse($user->updated_at)->toDateTimeString(),
            'created_at' => Carbon::parse($user->created_at)->toDateTimeString(),
            'id' => $user->id
		];
		
		return response()->json($response, 200);
    }
}
