<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Exception;

class PostController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Post $post)
    {
		$post = Auth::check() ? $request->user()->posts(): $post;

        return PostResource::collection($post->paginate(15));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request, Post $post)
    {
        $postdata = $request->validated();
        $postdata['slug'] = Str::slug($request->title, '-');
        $postdata['image'] = $request->image ?? 'https://www.seekpng.com/png/full/423-4235598_no-image-for-noimage-icon.png';
        $post = $request->user()->posts();
		$result = $post->create($postdata);
		
		$posts = $post->where('title',$request->title)->get();
        if( $posts->count() > 1 ) {
            foreach ($posts as $key => $post) {
                $post->update([
                    'slug' => Str::slug($request->title, '-') . (++$key)
                ]);
            }
        }

        if( $result->exists ) {
			return response()->json(new PostResource($result), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        try {
            $post = Post::where('slug', $slug)->firstOrFail();
            $post = Auth::check() ? $request->user()->posts()->where('slug', $slug)->firstOrFail() : $post;
			return response()->json(["data" => new PostResource($post)], 200);
			
        } catch (Exception $exception) {
			return response()->json([
				'message' => $exception->getMessage()
			], 404);
			
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        try {
			$post = Post::where('slug', $slug)->firstOrFail();
            $slug = $request->title ? Str::slug($request->title, '-') : $slug;
            $request['slug'] = $slug;

			$result = $post->update($request->all());

			$posts = $post->where('title',$request->title)->get();
			if( $posts->count() > 1 ) {
				foreach ($posts as $key => $post) {
					$post->update([
						'slug' => Str::slug($request->title, '-') . (++$key)
					]);
				}
			}

			return response()->json(["data" => new PostResource($post)], 200);
			
        } catch (Exception $exception) {
			return response()->json([
				'message' => $exception->getMessage()
			], 404);
			
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $slug)
    {
        try {
            $post = Post::where('slug', $slug)->firstOrFail()->delete();
			return response()->json(['status' => 'record deleted successfully'], 200);
			
        } catch (Exception $exception) {
			return response()->json([
				'message' => $exception->getMessage()
			], 404);
			
        }
    }
}
