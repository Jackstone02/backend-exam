# Getting started
## Installation

API Developed in Laravel.
* Register
* Login 
* Posts (create, show, update, delete)
* Comments (create, show, update, delete)

Clone the repository

	git clone [repository link]

Switch to the repo folder

	cd exam

Install all the dependencies using composer

	composer install

Copy the example env file and make the required configuration changes in the .env file

	cp .env.example .env

Generate a new application key

	php artisan key:generate

Run the database migrations (Set the database connection in .env before migrating)

	php artisan migrate

Run this command to create the encryption keys needed to generate secure access tokens

    php artisan passport:install

Start the local development server

	php artisan serve

You can now access the server at http://localhost:8000

TL;DR command list

	git clone [repository link]
	cd exam
	composer install
	cp .env.example .env
	php artisan key:generate

Make sure you set the correct database connection information before running the migrations Environment variables

	php artisan migrate
    php artisan passport:install
    php artisan serve